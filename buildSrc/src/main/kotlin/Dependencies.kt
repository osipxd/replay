import org.gradle.kotlin.dsl.DependencyHandlerScope

private const val KTOR_VERSION = "1.0.0-beta-3"
private const val KOIN_VERSION = "1.0.1"

fun DependencyHandlerScope.ktor(module: String): String {
    return "io.ktor:ktor-$module:$KTOR_VERSION"
}

fun DependencyHandlerScope.koin(module: String): String {
    return "org.koin:koin-$module:$KOIN_VERSION"
}
