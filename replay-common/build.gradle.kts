plugins {
    kotlin("platform.common")
}

dependencies {
    implementation(kotlin("stdlib"))
    implementation(ktor("http"))
    testCompile(kotlin("test"))
}
