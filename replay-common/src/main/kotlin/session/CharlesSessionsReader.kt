package ru.endlesscode.replay.session

import ru.endlesscode.replay.session.charles.CharlesSession

expect class CharlesSessionsReader {
    fun read(fileName: String): CharlesSession
}
