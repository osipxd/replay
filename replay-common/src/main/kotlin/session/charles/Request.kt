package ru.endlesscode.replay.session.charles

data class Request(
    val headers: Headers,
    val body: Body?
)
