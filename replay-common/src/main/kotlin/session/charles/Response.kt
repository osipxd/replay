package ru.endlesscode.replay.session.charles

import io.ktor.http.HttpStatusCode

data class Response(
    val status: HttpStatusCode,
    val headers: Headers,
    val body: Body?
)
