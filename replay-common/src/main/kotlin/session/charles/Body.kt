package ru.endlesscode.replay.session.charles

import io.ktor.http.ContentType
import kotlinx.io.charsets.Charset

data class Body(
    val type: ContentType,
    val charset: Charset,
    val content: String
)
