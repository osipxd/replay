package ru.endlesscode.replay.session.charles

import io.ktor.http.HttpMethod

data class TransactionId(
    val method: HttpMethod,
    val path: String
)
