package ru.endlesscode.replay.session.charles

class CharlesSession(
    val transactions: List<Transaction>
) : Iterable<Transaction> {

    companion object {
        val Empty: CharlesSession = CharlesSession(emptyList())
    }

    override fun iterator(): Iterator<Transaction> = transactions.iterator()

    override fun toString(): String {
        return "CharlesSession(${transactions.size} transactions)"
    }
}
