package ru.endlesscode.replay.session.charles

import io.ktor.http.HttpMethod

class Transaction(
    val method: HttpMethod,
    val path: String,
    val query: String,
    val duration: Long,
    val request: Request,
    val response: Response
) {

    override fun toString(): String {
        return buildString {
            append("Transaction(").append(method.value).append(' ').append(path)
            if (query.isNotEmpty()) append('?').append(query)
            append(" [").append(duration.toString()).append("ms])")
        }
    }
}
