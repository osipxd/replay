package ru.endlesscode.replay

import ru.endlesscode.replay.session.charles.CharlesSession
import ru.endlesscode.replay.session.charles.Transaction
import ru.endlesscode.replay.session.charles.TransactionId

class Mocks(session: CharlesSession) {

    private val mapping: Map<TransactionId, Transaction>

    init {
        mapping = mutableMapOf()

        for (transaction in session) {
            val id = TransactionId(transaction.method, transaction.path)
            mapping.put(id, transaction)
        }
    }

    fun getTransaction(id: TransactionId): Transaction? {
        return mapping[id]
    }
}
