import com.github.jengelman.gradle.plugins.shadow.tasks.ShadowJar
import org.jetbrains.kotlin.gradle.tasks.KotlinCompile

plugins {
    application
    kotlin("platform.jvm")
    id("com.github.johnrengelman.shadow") version "4.0.2"
}

application {
    mainClassName = "ru.endlesscode.replay.Main"
}

tasks.withType<ShadowJar> {
    manifest {
        attributes.apply {
            put("Implementation-Title", "Replay")
            put("Implementation-Version", version)
            put("Main-Class", application.mainClassName)
        }
    }
}

java {
    sourceCompatibility = JavaVersion.VERSION_1_8
}

tasks.withType<KotlinCompile> {
    kotlinOptions.jvmTarget = "1.8"
}

dependencies {
    val logbackVersion = "1.2.3"

    expectedBy(project(":replay-common"))
    implementation(kotlin("stdlib-jdk8"))
    implementation(ktor("server-cio"))
    implementation(koin("ktor"))
    implementation("ch.qos.logback:logback-classic:$logbackVersion")
    testCompile(kotlin("test-junit5"))
}
