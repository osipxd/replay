package ru.endlesscode.replay.xml

import java.io.InputStream
import javax.xml.stream.XMLInputFactory
import javax.xml.stream.XMLStreamReader
import javax.xml.stream.events.XMLEvent

class XmlReader(reader: XMLStreamReader) : XMLStreamReader by reader, Iterator<Int>, AutoCloseable {

    companion object {
        private val FACTORY by lazy { XMLInputFactory.newInstance() }
    }

    val attributes by lazy { Attributes() }

    private val isCData: Boolean
        get() = eventType == XMLEvent.CDATA

    constructor(stream: InputStream) : this(FACTORY.createXMLStreamReader(stream))

    inline fun <T> readElement(name: String, parent: String = "", read: XmlReader.() -> T): T? {
        if (!toStartOf(name, parent)) return null
        return read.invoke(this)
    }

    fun readData(name: String, parent: String = ""): String {
        val data = readDataOrNull(name, parent) ?: error("Data of $name shouldn't be null")
        if (isCharacters || isCData) return data
        error("Event should be CHARACTERS or CDATA event")
    }

    fun readDataOrNull(name: String, parent: String = ""): String? {
        toStartOf(name, parent)
        next()
        return if (isCharacters || isCData) text else null
    }

    fun toStartOf(name: String, parent: String = ""): Boolean {
        for (event in this) {
            if (isEndOf(parent)) return false
            if (isStartOf(name)) return true
        }
        return false
    }

    private fun isStartOf(name: String): Boolean {
        return name.isNotEmpty() && isStartElement && name == localName
    }

    private fun isEndOf(name: String): Boolean {
        return name.isNotEmpty() && isEndElement && name == localName
    }


    inner class Attributes {

        fun getValue(key: String): String {
            return get(key) ?: error("Attribute '$key' doesn't exists")
        }

        operator fun get(key: String): String? {
            return getAttributeValue(null, key)
        }
    }
}
