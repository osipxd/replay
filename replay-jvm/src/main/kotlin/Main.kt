@file:JvmName("Main")

package ru.endlesscode.replay

import io.ktor.application.Application
import io.ktor.application.call
import io.ktor.application.install
import io.ktor.features.CallLogging
import io.ktor.features.StatusPages
import io.ktor.http.HttpStatusCode
import io.ktor.response.respond
import io.ktor.routing.Routing
import io.ktor.routing.route
import io.ktor.routing.routing
import io.ktor.server.cio.CIO
import io.ktor.server.engine.commandLineEnvironment
import io.ktor.server.engine.embeddedServer
import org.koin.ktor.ext.get
import org.koin.ktor.ext.installKoin
import ru.endlesscode.replay.di.Properties
import ru.endlesscode.replay.di.mockModule


@Suppress("unused")
fun Application.main() {
    install(CallLogging)
    install(StatusPages) {
        exception<Throwable> { call.respond(HttpStatusCode.InternalServerError) }
    }

    installKoin()
    routing {
        mock()
    }
}

private fun Application.installKoin() {
    installKoin(
        listOf(mockModule),
        extraProperties = mapOf(
            Properties.SESSIONS_PATH to "sessions",
            Properties.SESSION_NAME to "wiki"
        )
    )
}

private fun Routing.mock() {
    val mocks = get<Mocks>()

    route("mock/{path...}") {
        handle { mocks.mock(call) }
    }
}


fun main(args: Array<String>) {
    embeddedServer(CIO, commandLineEnvironment(args)).start(wait = true)
}
