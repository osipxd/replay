package ru.endlesscode.replay.di

import org.koin.dsl.module.module
import org.koin.experimental.builder.factory
import ru.endlesscode.replay.Mocks
import ru.endlesscode.replay.session.CharlesSessionsReader

object Properties {
    const val SESSIONS_PATH = "sessions-path"
    const val SESSION_NAME = "session-name"
}

val mockModule = module {
    single { CharlesSessionsReader(getProperty(Properties.SESSIONS_PATH)) }
    factory<Mocks>()
    factory { get<CharlesSessionsReader>().read(getProperty(Properties.SESSION_NAME)) }
}
