package ru.endlesscode.replay

import io.ktor.application.ApplicationCall
import io.ktor.http.HttpStatusCode
import io.ktor.request.httpMethod
import io.ktor.response.respond
import io.ktor.response.respondText
import kotlinx.coroutines.delay
import ru.endlesscode.replay.session.charles.Transaction
import ru.endlesscode.replay.session.charles.TransactionId

suspend fun Mocks.mock(call: ApplicationCall) {
    val callStart = System.currentTimeMillis()
    val path = call.parameters.getAll("path")
        .orEmpty()
        .joinToString(separator = "/", prefix = "/")
    val id = TransactionId(call.request.httpMethod, path)
    val transaction: Transaction = getTransaction(id) ?: run {
        call.respond(HttpStatusCode.NotFound)
        return
    }

    val delayValue = transaction.duration - (System.currentTimeMillis() - callStart)
    if (delayValue > 0) delay(delayValue)

    val response = transaction.response
    if (response.body == null) {
        call.respond(response.status)
        return
    }

    val body = response.body
    call.respondText(body.content, body.type, response.status)
}
