package ru.endlesscode.replay.session

import io.ktor.http.ContentType
import io.ktor.http.HttpMethod
import io.ktor.http.HttpStatusCode
import kotlinx.io.charsets.Charset
import kotlinx.io.charsets.Charsets
import ru.endlesscode.replay.session.charles.Body
import ru.endlesscode.replay.session.charles.CharlesSession
import ru.endlesscode.replay.session.charles.Headers
import ru.endlesscode.replay.session.charles.Request
import ru.endlesscode.replay.session.charles.Response
import ru.endlesscode.replay.session.charles.Transaction
import ru.endlesscode.replay.xml.XmlReader
import java.nio.file.Files
import java.nio.file.Path
import java.nio.file.Paths

actual class CharlesSessionsReader(pathToSessions: String) {

    companion object {
        private const val CHARLES_SESSION = "charles-session"
        private const val TRANSACTION = "transaction"
        private const val REQUEST = "request"
        private const val RESPONSE = "response"
        private const val HEADERS = "headers"
        private const val ONE_HEADER = "header"
    }

    private val sessionsDir: Path = Files.createDirectories(Paths.get(pathToSessions))

    actual fun read(fileName: String): CharlesSession {
        val file = sessionsDir.resolve("$fileName.chlsx")
        val stream = Files.newInputStream(file)

        return XmlReader(stream).use { it.readCharlesSession() }
    }

    private fun XmlReader.readCharlesSession(): CharlesSession {
        return readElement(CHARLES_SESSION) {
            val transactions = mutableListOf<Transaction>()
            while (true) transactions.add(readTransaction() ?: break)
            CharlesSession(transactions)
        } ?: CharlesSession.Empty
    }

    private fun XmlReader.readTransaction(): Transaction? {
        return readElement(TRANSACTION, CHARLES_SESSION) {
            // Skip incomplete transactions
            while (attributes.getValue("status") != "COMPLETE") {
                if (!toStartOf(TRANSACTION, CHARLES_SESSION)) return null
            }

            Transaction(
                method = HttpMethod.parse(attributes.getValue("method")),
                path = attributes.getValue("path"),
                query = attributes["query"].orEmpty(),
                duration = attributes.getValue("duration").toLong(),
                request = readRequest(),
                response = readResponse()
            )
        }
    }

    private fun XmlReader.readRequest(): Request {
        return readElement(REQUEST, TRANSACTION) {
            val type = parseMimeType()
            val charset = parseCharset()

            Request(
                headers = readHeaders(REQUEST),
                body = type?.let { Body(it, charset, readData("body", REQUEST)) }
            )
        } ?: missingElement(TRANSACTION, REQUEST)
    }

    private fun XmlReader.readResponse(): Response {
        return readElement(RESPONSE, TRANSACTION) {
            val status = attributes.getValue("status").toInt()
            val type = parseMimeType()
            val charset = parseCharset()

            Response(
                status = HttpStatusCode.fromValue(status),
                headers = readHeaders(RESPONSE),
                body = type?.let { Body(it, charset, readData("body", RESPONSE)) }
            )
        } ?: missingElement(TRANSACTION, RESPONSE)
    }

    private fun XmlReader.readHeaders(parent: String): Headers {
        return readElement(HEADERS, parent) {
            mutableMapOf<String, String>().apply {
                while (true) {
                    val (name, value) = readHeader() ?: break
                    set(name, value)
                }
            }
        } ?: missingElement(HEADERS, parent)
    }

    private fun XmlReader.readHeader(): Pair<String, String>? {
        return readElement(ONE_HEADER, HEADERS) {
            val name = readDataOrNull("name", ONE_HEADER) ?: return null
            val value = readDataOrNull("value", ONE_HEADER) ?: return null

            return name to value
        }
    }

    private fun XmlReader.parseMimeType(): ContentType? {
        return ContentType.parse(attributes["mime-type"] ?: return null)
    }

    private fun XmlReader.parseCharset(): Charset {
        return Charset.forName(attributes["charset"] ?: return Charsets.UTF_8)
    }

    private fun missingElement(element: String, parent: String): Nothing {
        error("Element '$parent' should contain '$element'")
    }
}
