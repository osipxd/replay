package session

import ru.endlesscode.replay.session.CharlesSessionsReader
import kotlin.test.BeforeTest
import kotlin.test.Test
import kotlin.test.assertEquals

internal class SessionReaderTest {

    // SUT
    lateinit var sessionReader: CharlesSessionsReader

    @BeforeTest
    fun setUp() {
        sessionReader = CharlesSessionsReader("src/test/testFiles/sessions/")
    }

    @Test
    fun `test session should be parsed successfully`() {
        // When
        val session = sessionReader.read("test")

        // Then
        assertEquals(5, session.transactions.size)
    }

}
