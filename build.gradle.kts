plugins {
    base
}

buildscript {
    repositories {
        mavenCentral()
    }

    dependencies {
        classpath(kotlin("gradle-plugin", "1.3.0"))
    }
}

allprojects {
    group = "ru.endlesscode"
    version = "0.1.0"

    repositories {
        mavenCentral()
        jcenter()
        maven { url = uri("https://dl.bintray.com/kotlin/ktor") }
    }
}

dependencies {
    subprojects.forEach { archives(it) }
}
